package bulkyshare.project.de.bulkyshare.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bulkyshare.project.de.bulkyshare.R;
import bulkyshare.project.de.bulkyshare.container.Bulk;
import bulkyshare.project.de.bulkyshare.container.Contact;
import bulkyshare.project.de.bulkyshare.container.Termin;
import bulkyshare.project.de.bulkyshare.dialogs.AddBulkyItemDialog;
import bulkyshare.project.de.bulkyshare.dialogs.AddContactItemDialog;
import bulkyshare.project.de.bulkyshare.dialogs.ContactListDialog;
import bulkyshare.project.de.bulkyshare.tasks.TaskHandler;
import bulkyshare.project.de.bulkyshare.views.OnListViewTouch;
import bulkyshare.project.de.bulkyshare.views.ScrollViewCalendar;

/**
 * @author Alexander Radtke
 * @version 1.0
 *
 * This class displays all available information for a pickup date. The class is used in two cases
 *
 */
public class PickupDateDetail extends Activity implements AdapterView.OnClickListener,
                AdapterView.OnItemClickListener {
    private Termin termin;
    private ArrayAdapter<String> listAdapter, contact_adapter;
    private ArrayList<Contact> phone, mail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pickup_date_detail);
        setTitle(R.string.date_detail_title);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        // Get the name of the previous activity to check if buttons need to be displayed
        String name = extras.getString("name");
        // Get the Object from PickupDatesNearby, that belongs to the selected item
        termin = (Termin) extras.getSerializable("termin_object");

        phone = new ArrayList<Contact>();
        mail = new ArrayList<Contact>();
        // Load all bulky items and contacts from the database with the pickup date id
        try {
            TaskHandler taskHandler = new TaskHandler(this);
            taskHandler.setPara(new int[]{termin.getId()});
            termin.setBulk((ArrayList<Bulk>) taskHandler.execute("getBulks").get());
            if (termin.getBulk().size() == 0) {
                termin.setBulk(null);
            }
            taskHandler = new TaskHandler(this);
            taskHandler.setPara(new int[]{termin.getId()});
            termin.setContacts((ArrayList<Contact>) taskHandler.execute("getContacts").get());
            // Disable the CalendarView.
            // There is no need to make it scrollable because it only displays a date.
            ScrollViewCalendar calendarView = (ScrollViewCalendar) findViewById(R.id.collection_date);
            calendarView.setEnabled(false);
            calendarView.setDate(termin.getDateLong(), true, true);

            // Check for the cost_type and set it visible
            if (termin.getCost_type() != null) {
                RelativeLayout cat_pickup_price = (RelativeLayout) findViewById(R.id.cat_pickup_price);
                cat_pickup_price.setVisibility(View.VISIBLE);
                TextView cost_type = (TextView) findViewById(R.id.collection_cost_type);
                cost_type.setText(termin.getCost_type());
            }
            // Check for the price an set it visible
            if (termin.getPrice() != 0) {
                TextView price = (TextView) findViewById(R.id.collection_price);
                price.setText(new Integer(termin.getPrice()).toString() + " " + getString(R.string.currency));
            }
            // Check for the Bulk-list and set the Components visible
            if (termin.getBulk() != null) {
                RelativeLayout cat_pickup_bulk = (RelativeLayout) findViewById(R.id.cat_pickup_bulk);
                cat_pickup_bulk.setVisibility(View.VISIBLE);
                ListView bulk_list = (ListView) findViewById(R.id.collection_listview);

                String[] names = new String[termin.getBulk().size()];
                for (int i = 0; i < termin.getBulk().size(); i++) {
                    names[i] = termin.getBulk().get(i).getName();
                }

                listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
                bulk_list.setAdapter(listAdapter);
                bulk_list.setOnTouchListener(new OnListViewTouch());
                bulk_list.setOnItemClickListener(this);
            }
            // Check if there is any extra information for this pickup date
            if (!termin.getExtra_info().isEmpty()) {
                RelativeLayout cat_pickup_extra_info = (RelativeLayout) findViewById(R.id.cat_pickup_extra_info);
                cat_pickup_extra_info.setVisibility(View.VISIBLE);
                TextView extra_value = (TextView) findViewById(R.id.collection_extra_information_value);

                extra_value.setText(termin.getExtra_info());
            }
            // If the previous view was "Pickup dates nearby" show the two contact buttons.
            // These buttons are needed when a user wants to contact a pickup date creator
            if (name.equals(getString(R.string.collection_dates_nearby_title))) {
                LinearLayout button_layout = (LinearLayout) findViewById(R.id.button_layout);
                button_layout.setVisibility(View.VISIBLE);
                Button contact_phone = (Button) findViewById(R.id.contact_by_phone);
                Button contact_mail = (Button) findViewById(R.id.contact_by_mail);

                splitContacts();
                if (mail.size() == 0) {
                    contact_mail.setVisibility(View.GONE);
                } else if (phone.size() == 0) {
                    contact_phone.setVisibility(View.GONE);
                }

                contact_phone.setOnClickListener(this);
                contact_mail.setOnClickListener(this);
                // otherwise show the full contact ListView.
                // This ListView is needed when a pickup date creator wants to take a closer look at his own pickup date
            } else {
                RelativeLayout contacts_layout = (RelativeLayout) findViewById(R.id.cat_pickup_contacts);
                contacts_layout.setVisibility(View.VISIBLE);
                ListView contacts_list = (ListView) findViewById(R.id.contacts_listview);

                String[] contacts = new String[termin.getContacts().size()];
                for (int i = 0; i < termin.getContacts().size(); i++) {
                    contacts[i] = termin.getContacts().get(i).getContact();
                }

                contact_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contacts);
                contacts_list.setAdapter(contact_adapter);
                contacts_list.setOnTouchListener(new OnListViewTouch());
                contacts_list.setOnItemClickListener(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * There are two types of contacts that need to be separated.
     * This method splits all contact items, that were loaded from the database earlier, into mail
     * and phone contacts. This is achieved through the contact type.
     */
    private void splitContacts() {
        for (int i = 0; i < termin.getContacts().size(); i++) {
            if (termin.getContacts().get(i).getType().contains("E-Mail")) {
                mail.add(termin.getContacts().get(i));
            } else {
                phone.add(termin.getContacts().get(i));
            }
        }
    }

    /**
     * Handles clicks on the contact by phone and contact by mail buttons
     */
     @Override
     public void onClick(View view) {
         ContactListDialog listDialog;
         switch (view.getId()) {
             case R.id.contact_by_phone:
                 listDialog = new ContactListDialog(PickupDateDetail.this, "phone", phone);
                 listDialog.show();
                 break;
             case R.id.contact_by_mail:
                 listDialog = new ContactListDialog(PickupDateDetail.this, "mail", mail);
                 listDialog.show();
                 break;
             default:
                 break;
         }
     }

    /**
     * This listener method handles a click on a ListView item.
     * Since there are two ListViews, which items are clickable, we need to check their ids to
     * resolve which ListView and which item has been clicked.
     */
     @Override
     public void onItemClick(AdapterView adapterView, View view, int pos, long id) {
         switch (adapterView.getId()) {
             case R.id.collection_listview:
                 // Open a Dialog with the detail information about a Bulky Object
                 AddBulkyItemDialog show = new AddBulkyItemDialog(PickupDateDetail.this);
                 show.show();
                 show.setValuesForShow(termin.getBulk().get(pos));
                 break;
             case R.id.contacts_listview:
                 AddContactItemDialog show_contact = new AddContactItemDialog(PickupDateDetail.this);
                 show_contact.show();
                 show_contact.setValuesForShow(termin.getContacts().get(pos));
                 break;
             default:
                 break;
         }
     }
}
