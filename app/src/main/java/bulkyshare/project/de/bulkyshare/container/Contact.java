package bulkyshare.project.de.bulkyshare.container;

import java.io.Serializable;

public class Contact implements Serializable {
    private int id;
    private int termin_id;
    private String contact;
    private String type;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setTermin_id(int termin_id) {
        this.termin_id = termin_id;
    }

    public int getTermin_id() {
        return this.termin_id;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContact() {
        return this.contact;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }
}
