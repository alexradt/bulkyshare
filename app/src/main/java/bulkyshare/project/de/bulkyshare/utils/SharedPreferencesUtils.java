package bulkyshare.project.de.bulkyshare.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * SharedPreferencesUtils provides methods to read and write to the SharedPreferences file
 *
 * @author Alexander Radtke
 * @version 1.0
 */
public class SharedPreferencesUtils {
    private final static String PREF_FILE_NAME = "games_pref";
    private final static String USER_ID_KEY = "user_id";
    private final static String LAST_REFRESHED_KEY = "last_refreshed";
    private final static String LAST_REFRESHED_DATETIME_KEY = "last_refreshed_datetime";

    public static void writeToPreferences(Context context, String name, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(name, value);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String name, String defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(name, defaultValue);
    }

    public static String getUserId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_ID_KEY, "");
    }

    public static String getLastRefreshed(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(LAST_REFRESHED_KEY, "0000-00-00");
    }

    public static String getLastRefreshedDatetime(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(LAST_REFRESHED_DATETIME_KEY, "0000-00-00");
    }
}
