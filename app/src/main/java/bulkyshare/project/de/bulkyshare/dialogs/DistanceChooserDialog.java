package bulkyshare.project.de.bulkyshare.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import bulkyshare.project.de.bulkyshare.R;

/**
 * @author Alexander Radtke
 * @version 1.0
 *
 * With this Dialog the user has the possibility to set a maximum search range for pickup dates.
 * The step size is 0.2 kilometer and the maximum selectable distance is 25.0 kilometers which should
 * be more as enough for normal users.
 */
public class DistanceChooserDialog extends Dialog implements SeekBar.OnSeekBarChangeListener,
        AdapterView.OnClickListener {
    private Activity activity;
    private TextView current_distance;
    private double progressValue;

    public DistanceChooserDialog(Activity activity) {
        super(activity);
        this.activity = activity;
        progressValue = 0.2;
    }

    @Override
    public void onCreate(Bundle onSavedInstanceState) {
        super.onCreate(onSavedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.distance_chooser_dialog);

        // Load the saved distance from the SharedPreferences every time this dialog is called.
        SharedPreferences preferences = activity.getPreferences(Context.MODE_PRIVATE);
        progressValue = (double)preferences.getFloat("max_distance", new Float(progressValue));
        progressValue = Math.rint(progressValue * 10) / 10;

        current_distance = (TextView) findViewById(R.id.distance_current);
        current_distance.setText(progressValue + " " + activity.getString(R.string.distance_unit));

        SeekBar seekBar = (SeekBar) findViewById(R.id.distance_seek_bar);
        seekBar.incrementProgressBy(1);
        seekBar.setProgress((int)progressValue * 5);
        seekBar.setOnSeekBarChangeListener(this);

        Button save = (Button) findViewById(R.id.distance_save);
        save.setOnClickListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
        // Increments the distance in 0.2 steps until the maximum of 25 km is reached
        // The minimal selectable distance is 0.2 km
        if (progress == 0) {
            progressValue = 0.2;
        } else {
            progressValue = ((double) progress / 5.0);
        }
        current_distance.setText(progressValue + " " + activity.getString(R.string.distance_unit));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // Not needed
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // Not needed
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.distance_save:
                // Save the desired distance in the shared preferences
                SharedPreferences preferences = activity.getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putFloat("max_distance", new Float(progressValue));
                editor.commit();
                this.dismiss();
                break;
            default:
                break;
        }
    }
}
