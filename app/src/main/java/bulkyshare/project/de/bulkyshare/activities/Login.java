package bulkyshare.project.de.bulkyshare.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import bulkyshare.project.de.bulkyshare.R;
import bulkyshare.project.de.bulkyshare.container.Profil;
import bulkyshare.project.de.bulkyshare.tasks.TaskHandler;

/**
 * @author Philipp Viertel
 * @version 1.0
 *
 * This class handles the login-in screen. It provides the user a text field to type in
 * his username, which is a valid e-mail address, and his password.
 */
public class Login extends Activity implements View.OnClickListener {
    private EditText etUser, etPass;
    private TextView goToRegister;
    private Button bLogin;

    private String username, password;

    private TaskHandler taskHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login); // Changed to R.layout.login. Otherwise you will get a NullPointerException
        initialise();
    }

    private void initialise() {
        // With a live server, the following code segment is necessary

        etUser = (EditText) findViewById(R.id.etUser);
        etPass = (EditText) findViewById(R.id.etPass);
        bLogin = (Button) findViewById(R.id.bLogin);
        goToRegister = (TextView) findViewById(R.id.etRegister);

        bLogin.setOnClickListener(this);
        goToRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        username = etUser.getText().toString();
        password = etPass.getText().toString();

        if (view.getId() == R.id.etRegister) {
            Intent intent = new Intent(this, Registration.class);
            startActivity(intent);
        }
        // Login validation. Check if the user entered something in both text fields.
        // Check if data exists in the database and compare the passwords, if there are equal
        // forward the user to the MainActivity.

        if (!username.isEmpty() && !password.isEmpty()) {
            try {
                taskHandler = new TaskHandler(this);
                taskHandler.setParString(username);
                Profil profil = (Profil) taskHandler.execute("getProfilMail").get();
                Log.d("username", profil.getMail());
                if (profil != null && profil.getPassword().equals(password)) {
                    /*
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("user_id", Integer.toString(profil.getId()));
                    startActivity(intent);
                    finish();
                    */
                } else if (profil == null)  {
                    etUser.setError(getString(R.string.incorrect_email_address));
                } else if (!password.equals(profil.getPassword())) {
                    etPass.setError(getString(R.string.incorrect_password));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (username.isEmpty()) {
            etUser.setError(getString(R.string.email_address_empty));
        } else if (password.isEmpty()) {
            etPass.setError(getString(R.string.password_empty));
        }
    }
}
