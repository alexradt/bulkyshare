package bulkyshare.project.de.bulkyshare.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import bulkyshare.project.de.bulkyshare.R;
import bulkyshare.project.de.bulkyshare.container.Contact;

/**
 * @author Alexander Radtke
 * @version 1.0
 *
 * This Dialog supports multiple functionalities. There are methods
 * to show, add and edit a contact item.
 */
public class AddContactItemDialog extends Dialog implements AdapterView.OnClickListener {
    private Activity activity;
    private Button add, cancel;
    private EditText contact_value;
    private Spinner contact_type;
    private Contact contact;
    // Regular expression to verify a mail address that was entered by the user
    private final static String MAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public AddContactItemDialog(Activity activity) {
        super(activity);
        this.activity = activity;
        contact = new Contact();
    }

    @Override
    public void onCreate(Bundle onSavedInstanceState) {
        super.onCreate(onSavedInstanceState);
        // Hide the Dialog title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_contact_item);
        setCancelable(false);

        add = (Button) findViewById(R.id.add_contact);
        cancel = (Button) findViewById(R.id.cancel_contact);
        contact_value = (EditText) findViewById(R.id.contact_value);
        contact_type = (Spinner) findViewById(R.id.contact_type);

        add.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_contact:
                if (!TextUtils.isEmpty(contact_value.getText().toString())) {
                    if (contact_type.getSelectedItem().toString().equals("E-Mail")) {
                        if (contact_value.getText().toString().matches(MAIL_REGEX)) {
                            contact.setContact(contact_value.getText().toString());
                            contact.setType(contact_type.getSelectedItem().toString());
                            dismiss();
                        } else {
                            Toast.makeText(activity.getApplicationContext(), R.string.invalid_email_address, Toast.LENGTH_SHORT).show();
                        }
                    }
                    contact.setContact(contact_value.getText().toString());
                    contact.setType(contact_type.getSelectedItem().toString());
                    dismiss();
                } else {
                    Toast.makeText(activity.getApplicationContext(), R.string.fill_out_all_fields, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.cancel_contact:
                contact = null;
                cancel();
                break;
            default:
                break;
        }
    }

    /**
     * This method is used when the user wants to edit a contact.
     */
    public void setValuesForEdit(Contact contact) {
        contact_value.setText(contact.getContact());
        contact_type.setSelection(findItemInSpinner(contact_type, contact.getType()));
    }

    /**
     * This method is used to show all information about a contact
     */
    public void setValuesForShow(Contact contact) {
        TextView show_contact = (TextView) findViewById(R.id.show_contact_value);
        TextView show_contact_type = (TextView) findViewById(R.id.show_contact_type_value);

        show_contact.setVisibility(View.VISIBLE);
        show_contact_type.setVisibility(View.VISIBLE);

        show_contact.setText(contact.getContact());
        show_contact_type.setText(contact.getType());

        contact_value.setVisibility(View.GONE);
        contact_type.setVisibility(View.GONE);

        add.setVisibility(View.GONE);
        cancel.setVisibility(View.GONE);
        setCancelable(true);
    }

    /**
     * This method is needed to find and return the position of a contact type inside a spinner.
     */
    private int findItemInSpinner(Spinner spinner, String value) {
        int pos = 0;
        for (int i = 0; i < spinner.getAdapter().getCount(); i++) {
            if (spinner.getAdapter().getItem(i).equals(value)) {
                pos = i;
                return pos;
            }
        }
        return pos;
    }

    /**
     * Returns the contact, which is created when the user presses the add button
     */
    public Contact getContact() {
        return contact;
    }
}
