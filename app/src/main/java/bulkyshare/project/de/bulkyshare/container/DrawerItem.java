package bulkyshare.project.de.bulkyshare.container;

/**
 * @author Alexander Radtke
 * @version 1.0
 *
 *
 */
public class DrawerItem {
    private int id;
    private String name;

    public DrawerItem() {

    }

    public DrawerItem(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
