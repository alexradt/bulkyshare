package bulkyshare.project.de.bulkyshare.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import bulkyshare.project.de.bulkyshare.R;
import bulkyshare.project.de.bulkyshare.container.Profil;
import bulkyshare.project.de.bulkyshare.tasks.TaskHandler;

/**
 * @author Philipp Viertel
 * @version 1.0
 *
 * Class that provides all necessary functions for registration..
 */
public class Registration extends Activity implements View.OnClickListener {
    private EditText etUser, etPass, etPassRepeat;
    private Button bReg;

    private String username, password, passwordCheck;

    private static final String EMAILREGEXP = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                                  + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private TaskHandler taskHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        initialise();
    }

    private void initialise() {
        etUser = (EditText) findViewById(R.id.etUser);
        etPass = (EditText) findViewById(R.id.etPass);
        etPassRepeat = (EditText) findViewById(R.id.etPassRepeat);
        bReg = (Button) findViewById(R.id.bReg);

        bReg.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        username = etUser.getText().toString();
        password = etPass.getText().toString();
        passwordCheck = etPassRepeat.getText().toString();

        // Validation of the data...
        // Check if password and passwordCheck are identical
        if (!password.equals(passwordCheck)) {
            // The two passwords not identical!
            etPassRepeat.setError(getString(R.string.passwords_not_identical));
        }
        else if (password.equals("") || passwordCheck.equals("")) {
            // Password fields are empty
            etPass.setError(getString(R.string.password_empty));
            etPassRepeat.setError(getString(R.string.password_empty));
        }
        else if (password.length() < 5 || passwordCheck.length() < 5) {
            // Password should have at least 5 chars
            etPass.setError(getString(R.string.incorrect_password_length));
        }
        else if (!username.matches(EMAILREGEXP)) {
            // username is NOT a valid e-mail address
            etUser.setError(getString(R.string.invalid_email_address));
        }
        else if (username.equals("")) {
            // username is empty
            etUser.setError(getString(R.string.email_address_empty));
        }
        else {
            // everything should be fine
            try {
                taskHandler = new TaskHandler(this);
                taskHandler.setParString(username);
                Profil check_existence = (Profil) taskHandler.execute("getProfilMail").get();

                if (check_existence != null && check_existence.getMail().equals(username)) {
                    etUser.setError(getString(R.string.email_already_exists));
                } else {
                    Profil profil = new Profil(username, password);
                    taskHandler = new TaskHandler(this);
                    taskHandler.setProfil(profil);
                    taskHandler.execute("createProfil");
                    Toast.makeText(getApplicationContext(), R.string.registration_success, Toast.LENGTH_SHORT).show();
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
