package bulkyshare.project.de.bulkyshare.activities;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import bulkyshare.project.de.bulkyshare.R;
import bulkyshare.project.de.bulkyshare.fragments.NavigationDrawerFragment;

/**
 * @author Alexander Radtke
 * @version 1.0
 *
 *
 */
public class Navigator extends ActionBarActivity {
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigator);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // TODO: Change app icon size to 24dp. Should fit into the Toolbar then.
        //getSupportActionBar().setIcon(R.drawable.bulky_share_logo);
        drawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_fragment);
        drawerFragment.setUp(R.id.navigation_drawer_fragment, (DrawerLayout)findViewById(R.id.drawer_layout), toolbar);
    }
}
