package bulkyshare.project.de.bulkyshare.activities;

import java.util.ArrayList;
import java.util.List;

import bulkyshare.project.de.bulkyshare.container.Bulk;
import bulkyshare.project.de.bulkyshare.container.Contact;
import bulkyshare.project.de.bulkyshare.container.Profil;
import bulkyshare.project.de.bulkyshare.container.Termin;

/**
 * @author Philipp Viertel
 * @version 1.0
 *
 * Interface for DbHandler. When the server part is running on a "real" server, this class
 * is required for the remote access of the client (RMI).
 *
 */
public interface DbService {

    public void closeDB();
    public boolean openDB();

    public int createProfil(Profil p);
    public Profil getProfil(int profil_id);
    public Profil getProfil(String mail);
    public int updateProfil(Profil p);
    public void deleteProfil(int profil_id);

    public int createTermin(List<String> termin_list);
    public List<String> getTermin(int termin_id);
    public List<String> getAllTermine(int profil_id);
    public List<String> getAllTermineByProfile(int profil_id);
    public void deleteTermin(int termin_id);
    public int updateTermin(List<String> termin_list);

    public int createProfilTerminEntry(int profil_id, int termin_id);

    public List<Integer> createBulk(List<Bulk> bulkList, int termin_id);
    public List<Bulk> getBulks(int termin_id);
    public int updateBulk(Bulk b, int termin_id);
    public void deleteBulk(int bulk_id);

    public int createContact(Contact contact, int termin_id);
    public List<Contact> getContacts(int termin_id);
    public int updateContact(Contact contact, int termin_id);
    public void deleteContact(int contact_id);

    public String halloWelt();
}
