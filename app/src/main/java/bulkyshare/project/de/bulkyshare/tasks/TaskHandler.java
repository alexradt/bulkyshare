package bulkyshare.project.de.bulkyshare.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bulkyshare.project.de.bulkyshare.activities.DbService;
import bulkyshare.project.de.bulkyshare.container.Bulk;
import bulkyshare.project.de.bulkyshare.container.Contact;
import bulkyshare.project.de.bulkyshare.container.Profil;
import bulkyshare.project.de.bulkyshare.container.Termin;
import lipermi.handler.CallHandler;
import lipermi.net.Client;

public class TaskHandler extends AsyncTask<String, Void, Object> {

    private DbService dbServiceRemote;
    private CallHandler callHandler;

    private int par;
    private int[] para;
    private String parString;

    private List<Bulk> bulkList;
    private Contact contact;
    private List<String> termin;
    private Bulk bulk;
    private Profil profil;

    private Activity activity;
    //private ProgressDialog progressDialog;

    public TaskHandler(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        //progressDialog = new ProgressDialog(activity);
        //progressDialog.setMessage("Bitte warten...");
        //progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //progressDialog.setCancelable(false);
        //progressDialog.show();
    }

    @Override
    protected Object doInBackground(String... method) {

        callHandler = new CallHandler();

        try {
            //172.20.24.136
            Client client = new Client("172.20.24.136", 12345, callHandler);
            dbServiceRemote = (DbService) client.getGlobal(DbService.class);
            dbServiceRemote.openDB();
            for (String name : method) {
                if (name.equals("getProfilId")) {
                    // public Profil getProfil(int profil_id);
                    return dbServiceRemote.getProfil(para[0]);
                } else if (name.equals("getProfilMail")) {
                    // public Profil getProfil(String mail);
                    return dbServiceRemote.getProfil(parString);
                } else if (name.equals("deleteProfil")) {
                    // public void deleteProfil(int profil_id);
                    dbServiceRemote.deleteProfil(para[0]);
                } else if (name.equals("getTermin")) {
                    // public Termin getTermin(int termin_id);
                    List<String> termine =  dbServiceRemote.getTermin(para[0]);
                    return buildTermin(termine);
                } else if (name.equals("deleteTermin")) {
                    // public void deleteTermin(int termin_id);
                    dbServiceRemote.deleteTermin(para[0]);
                } else if (name.equals("createProfilTerminEntry")) {
                    // public int createProfilTerminEntry(int profil_id, int termin_id);
                    return dbServiceRemote.createProfilTerminEntry(para[0], para[1]);
                } else if (name.equals("deleteBulk")) {
                    // public void deleteBulk(int bulk_id);
                    dbServiceRemote.deleteBulk(para[0]);
                } else if (name.equals("deleteContact")) {
                    // public void deleteContact(int contact_id);
                    dbServiceRemote.deleteContact(para[0]);
                }

                else if (name.equals("getAllTermine")) {
                    // public List<Termin> getAllTermine();
                    List<String> termine =  dbServiceRemote.getAllTermine(para[0]);
                    return buildTerminList(termine);
                } else if (name.equals("getAllTermineByProfile")) {
                    // public List<Termin> getAllTermineByProfile(int profil_id);
                    List<String> termine = dbServiceRemote.getAllTermineByProfile(para[0]);
                    return buildTerminList(termine);
                } else if (name.equals("createBulk")) {
                    // public int[] createBulk(List<Bulk> bulkList, int termin_id);
                    return dbServiceRemote.createBulk(bulkList, para[0]);
                } else if (name.equals("getBulks")) {
                    // public List<Bulk> getBulks(int termin_id);
                    return dbServiceRemote.getBulks(para[0]);
                } else if (name.equals("getContacts")) {
                    // public List<Contact> getContacts(int termin_id);
                    return dbServiceRemote.getContacts(para[0]);
                }

                else if (name.equals("createProfil")) {
                    // public int createProfil(Profil p);
                    return dbServiceRemote.createProfil(profil);
                } else if (name.equals("updateProfil")) {
                    // public int updateProfil(Profil p);
                    return dbServiceRemote.updateProfil(profil);
                } else if (name.equals("createTermin")) {
                    // public int createTermin(Termin t);
                    return dbServiceRemote.createTermin(termin);
                } else if (name.equals("deleteTermin")) {
                    // public void deleteTermin(int termin_id);
                    dbServiceRemote.deleteTermin(par);
                } else if (name.equals("updateTermin")) {
                    // public int updateTermin(Termin t);
                    return dbServiceRemote.updateTermin(termin);
                } else if (name.equals("updateBulk")) {
                    // public int updateBulk(Bulk b, int termin_id);
                    return dbServiceRemote.updateBulk(bulk, par);
                } else if (name.equals("createContact")) {
                    // public int createContact(Contact contact, int termin_id);
                    return dbServiceRemote.createContact(contact, par);
                } else if (name.equals("updateContact")) {
                    // public int updateContact(Contact contact, int termin_id);
                    return dbServiceRemote.updateContact(contact, par);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Object obj) {
        dbServiceRemote.closeDB();
        //progressDialog.dismiss();
    }

    private List<Termin> buildTerminList(List<String> termine) {
        List<Termin> all_termine = new ArrayList<Termin>();
        int i = 1;
        int size = Integer.parseInt(termine.get(0));
        Log.d("size", Integer.toString(size));

        List<String> values = new ArrayList<String>();
        while (!termine.get(i).equals("ENDOFLIST")) {
            if (!termine.get(i).equals("END")) {
                values.add(termine.get(i));
                //Log.d("values", termine.get(i));
                i++;
            } else if (termine.get(i).equals("END")) {
                all_termine.add(buildTermin(values));
                values.clear();
                i++;
            }
        }
        return all_termine;
    }

    private Termin buildTermin(List<String> termin) {
        Termin termin_object = new Termin();
        termin_object.setId(Integer.parseInt(termin.get(0)));
        //Log.d("build_termin", Integer.toString(termin_object.getId()));
        termin_object.setDate(termin.get(1));
        //Log.d("build_termin", termin_object.getDate());
        termin_object.setStreet(termin.get(2));
        //Log.d("build_termin", termin_object.getStreet());
        termin_object.setPlz(termin.get(3));
        //Log.d("build_termin", termin_object.getPlz());
        termin_object.setCost_type(termin.get(4));
        //Log.d("build_termin", termin_object.getCost_type());
        termin_object.setPrice(Integer.parseInt(termin.get(5)));
        //Log.d("build_termin", Integer.toString(termin_object.getPrice()));
        termin_object.setExtra_info(termin.get(6));
        //Log.d("build_termin", termin_object.getExtra_info());
        termin_object.setLongitude(Double.parseDouble(termin.get(7)));
        termin_object.setLatitude(Double.parseDouble(termin.get(8)));
        return termin_object;
    }

    public void setParString(String parString) {
        this.parString = parString;
    }

    public void setPara(int para[]) {
        this.para = para;
    }

    public void setPar(int par) {
        this.par = par;
    }

    public void setObjList(List<Bulk> bulkList) {
        this.bulkList = bulkList;
    }

    public void setBulk(Bulk bulk) {
        this.bulk = bulk;
    }

    public void setTermin(List<String> termin) {
        this.termin = termin;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }
}
