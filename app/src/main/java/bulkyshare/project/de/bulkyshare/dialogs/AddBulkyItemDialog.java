package bulkyshare.project.de.bulkyshare.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import bulkyshare.project.de.bulkyshare.R;
import bulkyshare.project.de.bulkyshare.container.Bulk;

/**
 * @author Alexander Radtke
 * @version 1.0
 *
 * This Dialog supports multiple functionalities. There are methods
 * to show, add and edit a bulk item.
 */
public class AddBulkyItemDialog extends Dialog implements AdapterView.OnClickListener {
    private Activity activity;
    private EditText name, description, condition;
    private Button add, cancel;
    private Bulk item;

    public AddBulkyItemDialog(Activity activity) {
        super(activity);
        this.activity = activity;
        item = new Bulk();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // We don't want to have a title for our Dialog
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_bulky_item);
        // To dismiss this dialog the user has to press the cancel or the add button
        setCancelable(false);

        add = (Button) findViewById(R.id.add);
        cancel = (Button) findViewById(R.id.cancel);

        name = (EditText) findViewById(R.id.add_name_value);
        description = (EditText) findViewById(R.id.add_description_value);
        condition = (EditText) findViewById(R.id.add_condition_value);

        add.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.add:
                // All EditText-fields have to contain a value
                if (fieldsNotEmpty()) {
                    // create a bulk item when the user clicks the add button
                    item.setName(name.getText().toString());
                    item.setDescription(description.getText().toString());
                    item.setCondition(condition.getText().toString());
                    dismiss();
                } else {
                    // Show error message
                    Toast.makeText(activity.getApplicationContext(), R.string.fill_out_all_fields, Toast.LENGTH_SHORT).show();
                }
                break;
            // Quit the dialog
            case R.id.cancel:
                item = null;
                cancel();
                break;
            default:
                break;
        }
    }

    /**
     * Checks if there is no empty EditText-field
     * @return True if there is no empty EditText
     */
    private boolean fieldsNotEmpty() {
        if (!TextUtils.isEmpty(name.getText().toString()) &&
            !TextUtils.isEmpty(description.getText().toString()) &&
            !TextUtils.isEmpty(condition.getText().toString())) {
            return true;
        }
        return false;
    }

    /**
     * This method is used when the user wants to edit a bulk item.
     * All EditText-fields are filled with the given bulk item information.
     * @param bulk The bulk item that the user wants to edit.
     */
    public void setValuesForEdit(Bulk bulk) {
        name.setText(bulk.getName());
        description.setText(bulk.getDescription());
        condition.setText(bulk.getCondition());
        add.setText(R.string.save);
    }

    /**
     * This method is used to display all information about a bulk item.
     * All unneeded Elements are hidden and new elements are added through the visibility method.
     * @param bulk The bulk item the user wants to take a closer at.
     */
    public void setValuesForShow(Bulk bulk) {
        TextView show_name = (TextView) findViewById(R.id.show_name_value);
        TextView show_description = (TextView) findViewById(R.id.show_description_value);
        TextView show_condition = (TextView) findViewById(R.id.show_condition_value);

        show_name.setVisibility(View.VISIBLE);
        show_description.setVisibility(View.VISIBLE);
        show_condition.setVisibility(View.VISIBLE);

        show_name.setText(bulk.getName());
        show_description.setText(bulk.getDescription());
        show_condition.setText(bulk.getCondition());

        name.setVisibility(View.GONE);
        description.setVisibility(View.GONE);
        condition.setVisibility(View.GONE);

        add.setVisibility(View.GONE);
        cancel.setVisibility(View.GONE);
        setCancelable(true);
    }

    /**
     * Returns the bulk item, which is created when the user presses the add button
     */
    public Bulk getItem() {
        return item;
    }
}
