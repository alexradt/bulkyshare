package bulkyshare.project.de.bulkyshare.container;

import java.io.Serializable;

/**
 * @author Philipp Viertel
 * @version 1.0
 *
 * Container Class for a Profile.
 */
public class Profil implements Serializable {

    private int id;
    private String mail;
    private String password;

    public Profil() {}

    public Profil(String mail, String password) {
        this.mail = mail;
        this.password = password;
    }

    public Profil(int id, String mail, String password) {
        this.id = id;
        this.mail = mail;
        this.password = password;
    }

    // Getter & setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
