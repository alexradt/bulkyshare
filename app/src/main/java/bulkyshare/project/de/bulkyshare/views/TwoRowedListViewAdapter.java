package bulkyshare.project.de.bulkyshare.views;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import bulkyshare.project.de.bulkyshare.R;
import bulkyshare.project.de.bulkyshare.container.Termin;

/**
 * @author Alexander Radtke
 * @version 1.0
 *
 *
 */
public class TwoRowedListViewAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Termin> listItems;

    public TwoRowedListViewAdapter(Activity activity, List<Termin> listItems) {
        this.activity = activity;
        this.listItems = listItems;
    }

    /**
     * @return Returns the size of the List, that is given.
     */
    @Override
    public int getCount() {
        return listItems.size();
    }

    /**
     * @param location The position of an item in the List.
     * @return Returns one specific item from the List.
     */
    @Override
    public Object getItem(int location) {
        return listItems.get(location);
    }

    /**
     * @param pos The position of an item in the List.
     * @return The id of an item equals the position.
     */
    @Override
    public long getItemId(int pos) {
        return pos;
    }

    /**
     * getView is used to recycle View elements.
     * @param pos The position of an item in the List.
     * @param convertView convertView contains a View that can be recycled
     * @param parent The ParentView.
     * @return Returns a View item of the ListView
     */
    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        // if there is no View that can be recycled create a new one
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.two_rowed_listview, null);
        }

        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView distance_street = (TextView) convertView.findViewById(R.id.distance_street);

        title.setText(activity.getString(R.string.date) + " " + listItems.get(pos).getDate());
        if (listItems.get(pos).getDistance() != null) {
             distance_street.setText(activity.getString(R.string.distance) + " "
                     + listItems.get(pos).getDistance() + " " + activity.getString(R.string.distance_unit));
        } else {
            distance_street.setText(listItems.get(pos).getStreet() + ", " + listItems.get(pos).getPlz());
        }

        return convertView;
    }
}
