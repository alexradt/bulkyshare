package bulkyshare.project.de.bulkyshare.container;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * @author Alexander Radtke, Philipp Viertel
 * @version 1.0
 *
 * This class contains all the information about a Termin.
 */
public class Termin implements Serializable {
    private String date, street, plz, cost_type, extra_info;
    private double distance, longitude, latitude;
    private int price, id; // Added an id (like profil class)
    private ArrayList<Bulk> bulk;
    private ArrayList<Contact> contacts;

    public Termin() {

    }

    public Termin(String date, double distance, ArrayList<Bulk> bulk) {
        this.date = date;
        this.distance = distance;
        this.bulk = bulk;
    }

    public Termin(String date, double distance, String cost_type, int price, ArrayList<Bulk> bulk) {
        this.date = date;
        this.distance = distance;
        this.cost_type = cost_type;
        this.price = price;
        this.bulk = bulk;
    }

    public Termin(String date, String street, String plz) {
        this.date = date;
        this.street = street;
        this.plz = plz;
    }

    public String getDate() {
        return date;
    }

    public Long getDateLong() {
        Calendar cal = Calendar.getInstance();
        String[] cut = date.split("\\.");
        cal.set(new Integer(cut[2]), new Integer(cut[1]) - 1, new Integer(cut[0]));
        return cal.getTimeInMillis();
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDistance() {
        if (distance == 0.0) {
            return null;
        }
        return new Double(distance).toString();
    }

    public double getDistanceDouble() {
        return distance;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getCost_type() {
        return cost_type;
    }

    public void setCost_type(String cost_type) {
        this.cost_type = cost_type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ArrayList<Bulk> getBulk() {
        return bulk;
    }

    public void setBulk(ArrayList<Bulk> bulk) {
        this.bulk = bulk;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public String getExtra_info() {
        return extra_info;
    }

    public void setExtra_info(String extra_info) {
        this.extra_info = extra_info;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
