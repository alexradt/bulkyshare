package bulkyshare.project.de.bulkyshare.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;

import bulkyshare.project.de.bulkyshare.fragments.MyPickupDates;
import bulkyshare.project.de.bulkyshare.fragments.PickupDatesNearby;

/**
 * @author Alexander Radkte
 * @version 1.0
 *
 *
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private CharSequence mTitles[];
    private int numbOfTabs;

    public ViewPagerAdapter(FragmentManager fragmentManager, CharSequence mTitles[], int numbOfTabs) {
        super(fragmentManager);

        this.mTitles = mTitles;
        this.numbOfTabs = numbOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                PickupDatesNearby nearby = new PickupDatesNearby();
                return nearby;
            case 1:
                MyPickupDates myPickupDates = new MyPickupDates();
                return myPickupDates;
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }

    @Override
    public int getCount() {
        return numbOfTabs;
    }
}
