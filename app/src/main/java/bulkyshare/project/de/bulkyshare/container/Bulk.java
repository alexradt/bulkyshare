package bulkyshare.project.de.bulkyshare.container;

import java.io.Serializable;

/**
 * @author Alexander Radtke, Philipp Viertel
 * @version 1.0
 *
 *
 */
public class Bulk implements Serializable {
    private int id, termin_id;
    private String name, description, condition;

    public Bulk() {

    }

    public Bulk(String name, String description, String condition) {
        this.name = name;
        this.description = description;
        this.condition = condition;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTermin_id() {
        return termin_id;
    }

    public void setTermin_id(int termin_id) {
        this.termin_id = termin_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
