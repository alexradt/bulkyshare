package bulkyshare.project.de.bulkyshare.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bulkyshare.project.de.bulkyshare.R;

/**
 * @author Alexander Radtke
 * @version 1.0
 *
 * This class shows all the Collection dates, that the currently logged-in user, created.
 * The user can add new dates by clicking on the add symbol in the action bar.
 */
public class MyPickupDates extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_pickup_dates, container, false);

        return view;
    }
}
