package bulkyshare.project.de.bulkyshare.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.widget.CalendarView;

/**
 * Removes the same problem that appears with a ListView inside a ScrollView layout.
 * On Touch the ScrollView is intercepted, so the CalendarView can be scrolled up and down
 * to switch between months
 */
public class ScrollViewCalendar extends CalendarView {

    public ScrollViewCalendar(Context context) {
        super(context);
    }

    public ScrollViewCalendar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ScrollViewCalendar(Context context, AttributeSet attributeSet, int style) {
        super(context, attributeSet, style);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        ViewParent parent = getParent();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                parent.requestDisallowInterceptTouchEvent(true);
            case MotionEvent.ACTION_UP:
                parent.requestDisallowInterceptTouchEvent(true);
            default:
                return false;
        }
    }
}
