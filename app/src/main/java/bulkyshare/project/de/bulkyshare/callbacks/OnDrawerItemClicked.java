package bulkyshare.project.de.bulkyshare.callbacks;

import android.view.View;

/**
 * @author Alexander Radtke
 * @version 1.0
 */
public interface OnDrawerItemClicked {
    void itemClicked(View view, int position);
}
