package bulkyshare.project.de.bulkyshare.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bulkyshare.project.de.bulkyshare.R;

/**
 * @author Alexander Radtke
 * @version 1.0
 *
 * This class is the first View/Fragment to be shown after the login.
 * This class shows all pickup dates nearby depending on distance restrictions, that were set by the
 * user.
 * By touching an item the detail view of the item is displayed in an extra Activity.
 */
public class PickupDatesNearby extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pickup_dates_nearby, container, false);

        return view;
    }
}
