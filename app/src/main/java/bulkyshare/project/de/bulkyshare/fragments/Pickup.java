package bulkyshare.project.de.bulkyshare.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bulkyshare.project.de.bulkyshare.R;
import bulkyshare.project.de.bulkyshare.adapter.ViewPagerAdapter;
import bulkyshare.project.de.bulkyshare.views.SlidingTabLayout;

/**
 * @author Alexander Radtke
 * @version 1.0
 *
 *
 */
public class Pickup extends Fragment {
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private SlidingTabLayout tabLayout;
    private CharSequence titles[] = {"Nearby", "My Dates"};
    private int numbOfTabs = 2;

    public Pickup() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.pickup_date_slide, container, false);

        viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), titles, numbOfTabs);

        viewPager = (ViewPager)layout.findViewById(R.id.view_pager);
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout = (SlidingTabLayout) layout.findViewById(R.id.sliding_tabs);
        tabLayout.setDistributeEvenly(true);

        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorAccent);
            }
        });

        tabLayout.setViewPager(viewPager);

        return layout;
    }
}
