package bulkyshare.project.de.bulkyshare.adapter;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import bulkyshare.project.de.bulkyshare.R;
import bulkyshare.project.de.bulkyshare.callbacks.OnDrawerItemClicked;
import bulkyshare.project.de.bulkyshare.container.DrawerItem;
import bulkyshare.project.de.bulkyshare.fragments.NavigationDrawerFragment;

/**
 * RecyclerViewAdapter for the RecyclerView in the NavigationDrawer.
 *
 * @author Alexander Radtke
 * @version 1.0
 */
public class DrawerRecyclerView extends RecyclerView.Adapter<DrawerRecyclerView.DrawerViewHolder> {
    private LayoutInflater inflater;
    private List<DrawerItem> data = Collections.emptyList();
    private Context context;

    private int selectedPos = 0;
    private OnDrawerItemClicked onDrawerItemClicked;

    public DrawerRecyclerView(Context context, List<DrawerItem> data) {
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.context = context;
    }

    @Override
    public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.drawer_row, parent, false);
        DrawerViewHolder holder = new DrawerViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(DrawerViewHolder holder, int position) {
        DrawerItem current = data.get(position);
        holder.name.setText(current.getName());
        holder.icon.setImageResource(current.getId());

        holder.itemView.setSelected(selectedPos == position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setOnDrawerItemClicked(OnDrawerItemClicked onDrawerItemClicked) {
        this.onDrawerItemClicked = onDrawerItemClicked;
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder implements RecyclerView.OnClickListener {
        TextView name;
        ImageView icon;

        public DrawerViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.drawer_item_name);
            icon = (ImageView) itemView.findViewById(R.id.drawer_item_icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            notifyItemChanged(selectedPos);
            selectedPos = getPosition();
            notifyItemChanged(selectedPos);
            if (onDrawerItemClicked != null) {
                onDrawerItemClicked.itemClicked(v, getPosition());
            }
        }
    }
}
