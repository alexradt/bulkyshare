package bulkyshare.project.de.bulkyshare.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import bulkyshare.project.de.bulkyshare.R;
import bulkyshare.project.de.bulkyshare.container.Profil;
import bulkyshare.project.de.bulkyshare.tasks.TaskHandler;

/**
 * @author Alexander Radtke
 * @version 1.0
 *
 * Shows user specific information
 */
public class Profile extends Fragment implements AdapterView.OnClickListener {
    private String user_id;
    private TaskHandler taskHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle onSavedInstanceState) {
        View view = inflater.inflate(R.layout.profile, container, false);

        return view;
    }

    @Override
    public void onClick(View view) {
        deleteProfileDialog();
    }

    /**
     * This Dialog deletes the users profile completely and redirects him back to the login screen.
     * All of his pickup dates will also be deleted.
     */
    private void deleteProfileDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.delete_profile);
        builder.setMessage(R.string.delete_profile_message);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                taskHandler = new TaskHandler(getActivity());
                taskHandler.setPara(new int[]{new Integer(user_id)});
                taskHandler.execute("deleteProfil");
                getActivity().finish();
                startActivity(new Intent(getActivity(), Login.class));
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog delete = builder.create();
        delete.show();
    }
}
