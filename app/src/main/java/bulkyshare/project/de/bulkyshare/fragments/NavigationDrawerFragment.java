package bulkyshare.project.de.bulkyshare.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import bulkyshare.project.de.bulkyshare.R;
import bulkyshare.project.de.bulkyshare.activities.Profile;
import bulkyshare.project.de.bulkyshare.adapter.DrawerRecyclerView;
import bulkyshare.project.de.bulkyshare.callbacks.OnDrawerItemClicked;
import bulkyshare.project.de.bulkyshare.container.DrawerItem;
import bulkyshare.project.de.bulkyshare.utils.SharedPreferencesUtils;

/**
 * @author Alexander Radtke
 * @version 1.0
 *
 *
 */
public class NavigationDrawerFragment extends Fragment implements OnDrawerItemClicked {
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View containerView;
    private Toolbar toolbar;

    private RecyclerView recyclerView;
    private DrawerRecyclerView drawerRecyclerView;

    private final static String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";
    private boolean mUserLearnedDrawer;
    private boolean mFromSavedInstanceState;

    public NavigationDrawerFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // If the user never used this app before open the NavigationDrawer for him.
        mUserLearnedDrawer = Boolean.valueOf(
                SharedPreferencesUtils.readFromPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, "false"));
        if (savedInstanceState != null) {
            mFromSavedInstanceState = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        recyclerView = (RecyclerView)layout.findViewById(R.id.drawer_list);
        drawerRecyclerView = new DrawerRecyclerView(getActivity(), prepareData());
        drawerRecyclerView.setOnDrawerItemClicked(this);
        recyclerView.setAdapter(drawerRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        doTransaction(0);
        return layout;
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        this.toolbar = toolbar;
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!mUserLearnedDrawer) {
                    mUserLearnedDrawer = true;
                    SharedPreferencesUtils.writeToPreferences(
                            getActivity(), KEY_USER_LEARNED_DRAWER, Boolean.toString(mUserLearnedDrawer));
                }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }
        };
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            openDrawer();
        }
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public void openDrawer(){
        mDrawerLayout.openDrawer(containerView);
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout.isDrawerOpen(containerView);
    }

    private List<DrawerItem> prepareData() {
        List<DrawerItem> items = new ArrayList<>();
        int[] icons = {R.drawable.ic_local_shipping, R.drawable.ic_account_small,
                R.drawable.ic_settings, R.drawable.ic_exit_to_app};
        String[] names = getActivity().getResources().getStringArray(R.array.navigation_drawer_values);
        for (int i = 0; i < names.length; i++) {
            DrawerItem item = new DrawerItem();
            item.setId(icons[i]);
            item.setName(names[i]);
            items.add(item);
        }
        return items;
    }

    @Override
    public void itemClicked(View view, int position) {
        doTransaction(position);
        closeDrawer();
    }

    private void doTransaction(int position) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        switch (position) {
            case 0:
                transaction.replace(R.id.content_frame, new Pickup()).commit();
                break;
            case 1:
                transaction.replace(R.id.content_frame, new Profile()).commit();
                break;
            case 2:
                // Settings
                break;
            case 3:
                // Logout
                break;
        }
    }
}
