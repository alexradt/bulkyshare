package bulkyshare.project.de.bulkyshare.views;

import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;

/**
 * This class solves the scrolling problem with a ListView inside a ScrollView.
 * When the ListView is touched by the user and he scrolls up or down, the ScrollView
 * is disabled/intercepted so the ListView can be used normally. If the user isn't touching
 * the ListView the ScrollView is active.
 */
public class OnListViewTouch implements ListView.OnTouchListener {
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch(event.getAction()) {
            case MotionEvent.ACTION_UP:
                // Disallow ScrollView to intercept ListView scroll on up
                view.getParent().requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_DOWN:
                // Disallow ScrollView to intercept ListView scroll on down
                view.getParent().requestDisallowInterceptTouchEvent(true);
                break;
        }

        // Handle ListView touch events
        view.onTouchEvent(event);
        return true;
    }
}
